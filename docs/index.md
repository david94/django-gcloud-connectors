Django Gcloud Connectors (gcloudc)
==================================


The aim of this project is to create Django database connector / backend for Google Cloud.

Currently it contains a connector for the Google Cloud Datastore (Datastore in Firestore mode)
but in the future it may also include a Firestore connector, or even a MemoryStore one.

This is the continuation of the Datastore connector from the [Djangae project](https://github.com/potatolondon/djangae)
but converted to use the [Cloud Datastore API](https://googleapis.github.io/google-cloud-python/latest/datastore/) on Python 3.
It is deliberately split out into its own library so that if you want to you can use Django's ORM with the Cloud Datastore on any platform.
For example, you could run Django on AWS and still use this library to connect the ORM to your Google Cloud Datastore.
