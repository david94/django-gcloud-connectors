Installation
============

The most common way to install `gcloudc` is to use it as part of [Djangae](https://potato-oss.gitlab.io/djangae/djangae/).

You can also install it on its own with `pip install glcoudc`.

However you install it, you will need to set your `DATABASES` configuration in your Django settings module to look something like this:

```python
from djangae
DATABASES = {
    "default": {
        'ENGINE': 'gcloudc.db.backends.datastore',
        'PROJECT': 'YOUR_GCP_PROJECT_NAME',  # You can use djangae.environment.project_id() here
        'INDEXES_FILE': 'PATH_TO_A_FILE_FOR_SPECICAL_INDEXES',
    }
}
```

Note that the `INDEXES_FILE` here is _not_ the `index.yaml` file, but a separate file for gcloudc to store its "special indexes" in.
See [Special Indexes](concepts_limitations.md#special-indexes).


## Automatic Cloud Datastore Emulator startup

gcloudc provides overrides for the `runserver` and `test` commands which
start and stop a Cloud Datastore Emulator instance. To enable this functionality add `gcloudc.commands` _at the beginning_ of your `INSTALLED_APPS` setting.
