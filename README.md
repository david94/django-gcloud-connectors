# Django GCloud Connectors (gcloudc)

**Note: This project is now living in [GitLab](https://gitlab.com/potato-oss/google-cloud/django-gcloud-connectors)**

The aim of this project is to create Django database connector / backend for Google Cloud.

[Documentation](https://potato-oss.gitlab.io/google-cloud/django-gcloud-connectors/)

If you are interested in submitting a patch, please refer to the [Contributing](https://potato-oss.gitlab.io/google-cloud/django-gcloud-connectors/contributing) page.

---

## Looking for Commercial Support?

Potato offers Commercial Support for all its Open Source projects and we can tailor a support package to your needs.

If you're interested in commercial support, training, or consultancy then go ahead and contact us at [opensource@potatolondon.com](mailto:opensource@potatolondon.com)
