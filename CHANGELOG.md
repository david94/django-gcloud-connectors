Changelog
=========

## Development Version (targeting 0.4.0)

### New features & improvements
-
### Bug fixes
-
## v0.3.7

### New features & improvements

- Added support for using multiple values with `__contains` filters on iterable fields.

### Bug fixes

- Fix a bug where custom fields widget wouldn't work due to extra "renderer" param in the render method (introduced in Django 1.11)
- Fix a bug where querying on a decimal field wouldn't work

## v0.3.6

- N/A (changelog didn't exist before this version)
